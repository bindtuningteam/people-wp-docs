### Search Results

On **Search Results** you will need to set the filtering options you want for your results, the search value. You can also set a few of the 

First on the list is choosing which users appear on the page, by selecting one of the following options: 

![people_search.png](../images/modern/01.generaloptions.gif)
 
 - **Search all Users**: the web part will show all the users you have stored in your SharePoint People's directory.
  
 - **Search users from a Derpartment**: the web part will show all the users you have stored in your People's directory that belong to the department you specify on the **Search Value** field. 
 
 - **Search users from an Organization**: the web part will show all the users you have stored in your People's directory that belong to the organizaton you specify on the **Search Value** field. 
 
 - **Search users with a Job Title**: the web part will show all the users you have stored in your People's directory that have on their user information the job title you specify on the **Search Value** field. 

**Search Value**

Here you will need to specified the name of the department, organization or job title of the users you want the web part to show.

Check all the search values/managed properties you can use <a href="https://technet.microsoft.com/en-us/library/jj219630.aspx" target="_blank">here.</a> 

**Advanced Search**

You can add extra filtering conditions by clicking on **+ Advanced Search**. 

You will need to use **Keyword Query Language (KQL) syntax** to write all the values you want to add. <a href="https://docs.microsoft.com/en-us/sharepoint/dev/general-development/keyword-query-language-kql-syntax-reference" target="_blank">Read more.</a> 

Here is an example: 
    
    (contentclass=spspeople ContentClass=urn:content-class:SPSPeople) WorkEmail:bindtuning NOT WorkEmail:support
 
![people_advancedsearch.png](../images/classic/15.advancedsearch.png)



___

### List Results

On **List Results** you will need to set the lists you want to retrieve your users from.

1.  On the first field, place the link to a SharePoint site or list. On the dropdown to the side, you can select a list or one will be selected for you automatically.
2. On Filtering Options section, select how you want to filter your items
3. On the Field Mappings section, map the fields from the list with the fields of the web part
4. WHen you're done, lock your selection by clicking the save icon

![addList.gif](../images/classic/02.mapping.gif)

**Filtering options**

Using this dropdown, you can choose how you want to filter the items from your list.
- **No Filters** All the items from your list will be retrieved and displayed on the web part.
- **CAML Query**: This advanced option gives you full control over how you filter your results. Selecting this option, a text field appears to write your own CAML Query. We recommend the use of the free to use tool <a href="http://www.u2u.be/software/" target="_blank">U2U Caml Query Builder</a>, to help write CAML Queries.
- **Other Options** : Under the first 2 options, your list views will be listed. Select any list view and the content will be filtered accordingly.

**Field mapping options**

Under Field Mapping Options, you will find dropdowns for each of the web part's fields. These dropdowns include all the columns in your list's selected view. Select which column corresponds to which field or select **No Mapping** when nothing fits the bill. If you want to repeat the same column for different fields, that's ok too.

![List itens](../images/classic/16.cardoptions.png)