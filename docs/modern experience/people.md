### People Templates

You can choose from 6 different templates:

**Simple**

![simple](../images/classic/17.simple.png)

**Boxed**

![boxed](../images/classic/18.boxed.png)

**List**

![List](../images/classic/19.list.png)

**Face**

![face](../images/classic/20.face.png)

**Condensed List**

![Condensed](../images/classic/21.condensed.png)

**Tiny List**

![Tiny](../images/classic/22.tiny.png)

___
### Rows per page

Lets you select the maximum number of rows that can be created. The web part will only render the set number of rows if there's enough space to show everyone, it'll create pages where the other users will be visible. The number of columns is adjusted for the user's screen size.

___
### Search Bar

With this option a search box will appear at the top. You can use an email, user name to search the users.

___
### Alphabetic Search

With this option an alphabetic search bar will appear at the top. When you click it'll filter everyone where the First Name is equal to the selected character. 

___
### About

It'll show on the layout the field that is reserved for this About Section. 

___
### Profile Picture

Allows you to choose either or not to have the photos of the users visible.   

___
### Internal/External

This option is only available when the People Search is in use and will identify the users that are  Internal or External to the organization. 

___
### Email and send email

Allows you to choose either or not to have the email of the users visible. With that option selected, you'll unlock the option to allow or not to send an email when clicking on the personal email with the selected program of your device.      

___
### Phone and dial-in

Allows you to choose either or not to have the phone of the users visible. With that option selected, you'll unlock the option to allow or not to dial-in when clicking on the personal email with the selected program of your device.    

___
### Skype and send Skype message

Allows you to choose either or not to have the Skype email of your users visible. With that option selected, you'll unlock the option to allow or not to send an Skype message when clicking on the personal email with the selected program of your device.  

_______
### Teams

Allows you to choose either or not to Open the Microsoft Teams with the person that you've clicked selected so you a message to the person. 
___
### Choose Image Provider

This option is only available if you're using the [Search Results](./options.md) to display the users.
You can choose where the image from the People Web Part will get retrieved. The options available are:

1. Delve API
2. Delve
3. Outlook
4. MySites (OneDrive)
5. MySites (URL)
6. SharePoint 

By default, the option selected is **SharePoint**. 

<p class ="alert alert-info">Note: In some cases (Delve and OneDrive), the Users need to be logged in on the provider to get the images.</p>

___
### URL of My Sites

When you select the option **MySites (URL)** on Choose Image Provider you'll have access to insert your custom URL One-Drive.
___
### Tenant Location

If you choose Delve, you also need to give the information where your tenant is located so we can get the images from the Delve. The options available are: 

1. Europe (eur.delve.office.com)
2. America (nam.delve.office.com)
3. U.S. Government (delve-gcc.office.com)


![options](../images/modern/09.peopleoptions.png)