![theme](../images/modernx/38.theme.png)

- **Apply theme color on title** - Displays the web part title with the theme's primary color on background.