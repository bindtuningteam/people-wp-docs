### Export to CSV
Adds an option to export the current selection of people to a csv file.

![export_to_csv](../images/modernx/43.export_to_csv.png)

___
### Hover panel
When a person is hovered on, a small panel will appear, that later expands to a bigger panel, showing some additional information about that person.

![hover_panel](../images/modernx/44.hover_panel.png)

___
### Quick mail & Quick call
These options add each a button to each person card that allow to send an email or call directly to the corresponding person.

![email_and_call](../images/modernx/45.email_and_call.png)
