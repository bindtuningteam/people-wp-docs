Choose one of the 5 available layouts

![layout_options](../images/modernx/13.layout_options.png)

___
### Card color

You can choose a background color for the card

![palette_button](../images/modernx/16.palette_button.png) | ![free_picker_button](../images/modernx/17.free_picker_button.png)
------------- | -------------
![color_picker](../images/modernx/18.color_picker.png)  | ![color_picker2](../images/modernx/19.color_picker2.png)

The options are:

- **BindTuning Theme** - Choose a color from the BindTuning Theme palette, if a theme is applied.
- **Change the look** - Choose a color from the Change the look palette.
- **Free picker** - Choose any other color.

<p class="alert alert-info">
All selected colors will result in a lighter color, since it will automatically set an opacity of 15%
</p>

___
### Configure layout
Here you can configure the layout, by giving it new label names or new fields for each of the available positions.

![configure_layout](../images/modernx/15.configure_layout.png)
