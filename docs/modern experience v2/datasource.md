![data_source](../images/modernx/05.datasource.png)


Decide where the data will be stored. Inside the web part or on a SharePoint List:

- **Users and Groups** - All people will be stored on the context of the web part
- **Get data from list** - Connect the web part to a SharePoint lists and gets data from it.
- **SharePoint Search** - Gets all people from within SharePoint.
- **Connect to Azure AD** - Gets all people from Azure AD.

</br>

___
### Get data from list

![site_origin](../images/modernx/06.site_origin.png)


- **Current site** - Choose or create a list on the same collection.
- **Other sites** - Choose or create a list on any site collection.

</br>

#### Other sites
Search for a site or pick from frequent or recent sites

![site_picker](../images/modernx/07.choose_site.png)

</br>

Choose a list action

![list_Actions](../images/modernx/08.list_actions.png)

</br>

- **Create new** - Create a list on the selected site by entering a list name and click "Create list" button. After the list has been created, the web part will automatically be connected to that list.

![create_list](../images/modernx/09.create_list.png)

- **Existing list** - Search for an existing list.

![create_list](../images/modernx/10.existing_list.png)

</br>
<p class="alert alert-warning">Note: If the list was not created by the People web part you need to configure mappings.</p>
</br>

</br>

___
### Choose a view or filter with a caml query

Click on the cog weel

![create_list](../images/modernx/11.configure_mappings_button.png)


</br>
To filter the view, you have two options:

![create_list](../images/modernx/12.select_view.png)

</br>

- **Views** - Choose a view from available views.

- **Caml Query** - Apply a caml query.

#### Caml Query

You query must have the following structure

``` <View><Query><Where></Where></Query></View> ```

<p class="alert alert-info">
You can create caml queries using U2U Caml Query builder tool. Download it, <a href="https://www.u2u.be/software" target="blank">here</a>
</p>

</br>

___
### Filter Users

Both in SharePoint Search and Azure AD, you can filter the users.

On SharePoint Search you can either filter them by Department/Job Title, or, if you activate advanced mode, you can use a KQL string.

![filter_spsearch](../images/modernx/41.filter_spsearch.png)

On Azure AD, you can filter by enablement, user type, lincensing, active, creation type, or a custom filter from the dropdown.

![filter_azure_ad](../images/modernx/42.filter_azure_ad.png)